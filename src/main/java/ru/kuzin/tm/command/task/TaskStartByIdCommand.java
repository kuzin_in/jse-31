package ru.kuzin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.kuzin.tm.enumerated.Status;
import ru.kuzin.tm.util.TerminalUtil;

public final class TaskStartByIdCommand extends AbstractTaskCommand {

    @NotNull
    private static final String NAME = "task-start-by-id";

    @NotNull
    private static final String DESCRIPTION = "Start task by id.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        @NotNull final String userId = getUserId();
        System.out.println("[START TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        getTaskService().changeTaskStatusById(userId, id, Status.IN_PROGRESS);
    }

}